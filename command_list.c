//
// Created by danilo on 30/09/15.
//

#include "command_list.h"

//
// Created by danilo on 30/09/15.
//

#include <stddef.h>
//#include <malloc.h>
#include <stdlib.h>


void CommandList_insert(CommandList* list, Command c)
{
    CommandListNode** ptr = NULL;
    if (list->last == 0) {
        ptr = &(list->first);
    } else {
        ptr = &(list->last->next);
    }
    (*ptr) = malloc(sizeof(CommandListNode));
    (*ptr)->command    = c;
    (*ptr)->next = 0;
    list->last   = (*ptr);
    list->length += 1;
}

void CommandList_destroy(CommandList* list)
{
    CommandListNode* curr;
    CommandListNode* next;
    if (list->length > 0) {
        for (
                curr = list->first, next = curr->next;
                curr;
                curr = next, (next!=NULL) ? next = next->next : (next = 0)
                ) {
            free(curr);
        }
    }

    free(list);


}

CommandList* CommandList_new(void)
{
    CommandList* n = calloc(1, sizeof(CommandList));
    return n;
}

