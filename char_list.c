//
// Created by danilo on 30/09/15.
//

#include <stddef.h>
#include <stdlib.h>
#include "char_list.h"

struct _charlist_node
{
    char c;
    struct _charlist_node* next;
};

struct _charlist
{
    unsigned int length;
    struct _charlist_node* first;
    struct _charlist_node* last;
};

typedef struct _charlist_node StringListNode;


void CharList_insertChar(CharList* list, char c)
{
    StringListNode** ptr = NULL;
    if (list->last == 0) {
        ptr = &(list->first);
    } else {
        ptr = &(list->last->next);
    }
    (*ptr) = malloc(sizeof(struct _charlist_node));
    (*ptr)->c    = c;
    (*ptr)->next = 0;
    list->last   = (*ptr);
    list->length += 1;
}

void CharList_destroy(CharList* list)
{
    StringListNode* curr;
    StringListNode* next;
    if (list->length > 0) {
        for (
                curr = list->first, next = curr->next;
                curr;
                curr = next, (next!=NULL) ? next = next->next : (next = 0)
                ) {
            free(curr);
        }
    }

    free(list);


}

CharList* CharList_new(void)
{
    CharList* n = calloc(1, sizeof(CharList));
    return n;
}

char* CharList_toCharPtr(CharList* list)
{

    char* s  = calloc((size_t) list->length + 1, sizeof(char));
    char* vs = s;
    StringListNode* node;

    for (node = list->first; node; node = node->next) {
        *vs = node->c;
        vs++;
    }

    *vs = '\0';

    return s;
}
