//
// Created by danilo on 9/29/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "auxiliary.h"
#include "command_list.h"
#include <sys/shm.h>

#define MAX_PROCNAME 10


CommandList* parse(void)
{
    CommandList* l = CommandList_new();
    while (getc(stdin) != EOF) {
        fseek(stdin, -1, SEEK_CUR);
        Command c;
        if (processToken("exec", " \n\t") == EOF){  //process token `exec`
            break; //acabou o arquivo;
        }
        c.proc_name = processString(" \n\t="); //
        if (strlen(c.proc_name) == 0) {
            printf("Nome do processo era esperado!");
            assert(0);
        }

        CommandList_insert(l, c);
    }
    return l;
}

void checkCommands(CommandList* list) {
    CommandListNode* node;
    Command cmd;
    struct stat sb;
    int ok = 1;
    for (node = list->first; node; node = node->next) {
        cmd = node->command;
        if (!(stat(cmd.proc_name, &sb) == 0 && sb.st_mode & S_IXUSR)) {
            fprintf(stderr, "Arquivo `%s` não existe ou não é executável!\n", cmd.proc_name);
            ok = 0;
        }
    }
    if (!ok) {
        fprintf(stderr, "Alguns Arquivos não são válidos!\n\n");
        assert(ok);
    }


    /* non-executable */
}

int main(void)
{
    printf("Interpretador Começou! :-)\n");
    int *priority, mem1;
    char* procName;

    changeDefaultInputFile();
    changeDefaultOutputFile(0);

    CommandList* commands = parse();
    checkCommands(commands);


    key_t memKey1 = 8760;//    printf(memKey1, memKey2);

    mem1 = shmget(memKey1, (commands->length)* MAX_PROCNAME*sizeof(char), IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    if (mem1 == -1 ) {
        perror("Erro get memoria1 interpretador");
        exit(1);
    }


    procName = (char*) shmat(mem1, 0, 0);



//    char* str;
    CommandListNode* node = commands->first;
    for(int i = 0; i < commands->length; i++)
    {
        for (int j = 0; j <= strlen(node->command.proc_name); j++ ) {
            procName[i* MAX_PROCNAME + j] = node->command.proc_name[j];
        }
        printf("%s\n", (procName + i* MAX_PROCNAME));

        node = node->next;
    }


    printf("interpretador round_robin!\n");
    //closeAlternativeInput();a


    shmdt(procName);
//    shmdt(priority);

//    shmctl(mem1, IPC_RMID, 0);
//    shmctl(mem2, IPC_RMID, 0);



    char arg1[31], arg2[31];
    sprintf(arg1,"%d", mem1);
    sprintf(arg2,"%d", commands->length);

    printf("%s\n", arg1);
    printf("%s\n", arg2);


    char* const args[] = {"escalonador_round_robin", arg1,arg2,NULL};

    printf("iniciando escalonador!");
    if ( execve("./escalonador_round_robin", args ,NULL) == -1 ) {
        perror("Nao executou :( \n");
        shmctl(mem1, IPC_RMID, 0);
    }

    return 0;
}
