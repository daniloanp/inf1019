//
// Created by Tatiana Magdalena on 10/7/15.
//

//
// Created by danilo on 05/10/15.
//
#define _POSIX_SOURCE // apenas para código Linux não dar warning

#include <unistd.h>
#include <stdio.h>

#define EVER (;;)
int main(void) {
    for (int i = 0; i < 4; i++) {
        printf("\tQuinto %d! %d\n", i, getpid());
        sleep(3);
    };
    return 0;
}
