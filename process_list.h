//
// Created by danilo on 05/10/15.
//

#ifndef INF1019_PROCESS_LIST_H
#define INF1019_PROCESS_LIST_H


#include <stdbool.h>

typedef struct _procNode Process;

//typedef struct _procList ProcessList;


//struct _procList
//{
//    unsigned int length;
//    struct Process* first;
//    struct Process* last;
//};


struct _procNode
{
    int pid; // se -2, processo nao iniciou
    int priority;
    char* procName;
    bool ready;
    bool in_io;

    bool finished;

    //nao sendo usado
    Process* next;
    Process* previous;
};


Process* Process_new(); // só pra simplificar


#endif //INF1019_PROCESS_LIST_H

