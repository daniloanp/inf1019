//
// Created by danilo on 30/09/15.
//

#ifndef INF1019_COMMAND_LIST_H
#define INF1019_COMMAND_LIST_H

typedef struct {
    char *proc_name;
    int priority;
} Command;


struct _command_list_node
{
    Command command;
    struct _command_list_node* next;
};

struct _command_list
{
    unsigned int length;
    struct _command_list_node* first;
    struct _command_list_node* last;
};

typedef struct _command_list CommandList;
typedef struct _command_list_node CommandListNode;
//CommandList
void CommandList_insert(CommandList* list, Command c);

void CommandList_destroy(CommandList* list);

CommandList* CommandList_new(void);


#endif //INF1019_COMMAND_LIST_H
