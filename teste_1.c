//
// Created by danilo on 05/10/15.
//
#define _POSIX_SOURCE // apenas para código Linux não dar warning

#include <unistd.h>
#include <stdio.h>

#define EVER (;;)
int main(void) {

    setvbuf(stdout, NULL, _IONBF, 0);
    for EVER{
        printf("\tPrimeiro! %d\n", getpid());
        sleep(1);
    };
    return 0;
}