#define _POSIX_SOURCE // apenas para código Linux não dar warning

#include <unistd.h>
#include <stdio.h>

#define EVER (;;)
int main(void) {

    for (int i = 0; i < 5; i++) {
        printf("\t Sexta %d! %d\n", i, getpid());
        fprintf(stderr, "\t Sexta(quem dera!) %d! %d\n", i, getpid());
        sleep(2);
    };
    return 0;
}
