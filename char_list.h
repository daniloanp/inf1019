//
// Created by danilo on 30/09/15.
//

#ifndef INF1019_CHARLIST_H
#define INF1019_CHARLIST_H

typedef struct _charlist CharList;


void CharList_insertChar(CharList* list, char c);

void CharList_destroy(CharList* list);

CharList* CharList_new(void);

char* CharList_toCharPtr(CharList* list);


#endif //INF1019_CHARLIST_H
