//
// Created by danilo on 05/10/15.
//
#define _POSIX_SOURCE // apenas para código Linux não dar warning
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#define EVER (;;)

int main(void) {

    int i = 1;
    setvbuf(stdout, NULL, _IONBF, 0);

    for EVER {
        sleep(1); //tempo antes do IO;


        if (i % 2 == 0) {
            printf("\tTerceiro!(IO inicia) %d\n ", getpid());
            kill(getppid(), SIGIO);
            sleep(3);
            kill(getppid(), SIGIOT);
            printf("\tTerceiro!(IO acaba) %d\n ", getpid());
//            sleep(1); //tempo antes do IO;
            kill(getpid(), SIGSTOP);
        } else {
            printf("\tNormal! %d\n ", getpid());
            sleep(1);
        }

        i++;


    };
    return 0;
}
