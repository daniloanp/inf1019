//
// Created by danilo on 30/09/15.
//

#ifndef INF1019_AUXILIARY_H
#define INF1019_AUXILIARY_H


int changeDefaultInputFile();
int changeDefaultOutputFile(int type);
char* processString(const char* ignoring);
int processToken(const char* find, char* ignoring);

char* processStringInLine(char **line, const char* ignoring);
int processTokenInLine(char **line, const char* find, char* ignoring);
void closeAlternativeInput();
#endif //INF1019_AUXILIARY_H
