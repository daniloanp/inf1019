//
// Created by danilo on 30/09/15.
//

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
//#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "auxiliary.h"
#include "char_list.h"

#define INPUT_FILE "entrada.txt"
#define OUTPUT_FILE "saida.txt"
#define INPUT_DEFAULT_CODE  0
#define OUTPUT_DEFAULT_CODE 1

static int fdin = -1;
static int fdout = -1;

int changeDefaultInputFile() {
    int dupRes = -1;

    fdin = open(INPUT_FILE, O_RDONLY, 0666);
    if (fdin == -1) {
        perror("Nao foi possivel abrir o arquivo de entrada!");
        assert(0);
    }

    close(INPUT_DEFAULT_CODE);

    dupRes = dup(fdin);
    if (dupRes == -1) {
        perror("Nao foi possivel mudar a entrada padrão!");
        assert(0);
    }

    return fdin;
}

int changeDefaultOutputFile(int type) {
    //0 - interpretador
    //1 - escalonador

    int dupRes = -1;

    if (type == 0) {
        fdout = open(OUTPUT_FILE, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    }
    else {
        fdout = open(OUTPUT_FILE, O_WRONLY | O_CREAT | O_APPEND, 0666);
    }

    if (fdout == -1) {
        perror("Nao foi possivel abrir o arquivo de saida!");
        assert(0);
    }

    close(OUTPUT_DEFAULT_CODE);

    dupRes = dup(fdout);
    if (dupRes == -1) {
        perror("Nao foi possivel mudar a saida padrao!");
        assert(0);
    }

    return fdout;
}


void closeAlternativeInput() {
    close(fdin);
}


char *processString(const char *ignoring) {
    int c;
    char *res = 0;
    while ((c = getc(stdin)) != EOF) { // pulando caracteres a ignorar
        if (strchr(ignoring, c) != NULL) { //ignored
            continue;
        } else {
            break;
        }
    }
    CharList *l = CharList_new();
    if (c != EOF) {
        CharList_insertChar(l, (char) c);
    }
    while ((c = getc(stdin)) != EOF) {
        if (strchr(ignoring, c) != NULL) { //termina ao ler caracteres a ignorar
            fseek(stdin, -1, SEEK_CUR);
            break;
        }
        CharList_insertChar(l, (char) c);
    }

    res = CharList_toCharPtr(l);
    CharList_destroy(l);


    return res;
}

int processToken(const char *token, char *ignoring) {
    const char *find = token;
    int c;
    int started = 0;
    while ((c = getc(stdin)) != EOF) {

        if (strchr(ignoring, c) != NULL) { //ignored
            if (*find == '\0') {
                fseek(stdin, -1, SEEK_CUR);
                return 1;
            } else if (!started) {
                continue;
            }
        }
        if (*find == '\0') {
            fseek(stdin, -1, SEEK_CUR);
            return 1;
        }
        started = 1;
        if (c != *find) {
            printf("caracter `%c` inesperado para token %s! O esperadado era %c", c, token, *find);
            assert(0);
            return 0;
        }
        find++;
    }
    if (started) {
        printf("token %s era esperado. Nada encontrado!", token);
        assert(0);
    }


    return EOF;
}


