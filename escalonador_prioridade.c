//
// Created by danilo on 9/29/15.
//
//teste
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "auxiliary.h"
#include "command_list.h"
#include <sys/shm.h>
#include <stdbool.h>
#include <signal.h>
#include <fcntl.h>

#define MAX_PROCNAME 10
#define MAX_NPROC 10
#include "process_list.h"


int main(int argc, char *argv []) {
    //evita buffer de saída que pode ser copiado após fork.
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    fprintf(stderr, "\t->Entrou no escalonador.\n");

    changeDefaultInputFile();
    changeDefaultOutputFile(1);



    int numProc = 0;

    int mem1, mem2, mem3;
    int *priority, *sharedCount;
    char* procName;

    mem1 = atoi(argv[1]);
    mem2 = atoi(argv[2]);
    mem3 = atoi(argv[3]);

    printf("%d, %d, %d\n", mem1, mem2, numProc);

    procName = (char*) shmat(mem1, 0, 0);
    priority = (int*) shmat(mem2, 0, 0);
    sharedCount = (int*) shmat(mem3, 0, 0);

//    fprintf(stderr, "Dados pegos no escalonador: %s, %d, %d\n", procName, *priority, *sharedCount);

    Process* current = NULL;
    Process* procs = calloc((size_t) MAX_NPROC, sizeof(Process));

    pid_t new_pid;

    while(*sharedCount != -1) { //Enquanto ainda esta recebendo os processos. Crio um fork para cada um conforme recebo.

        fprintf(stderr, "\t->Checa se tem arquivo pra ler na mem.\n");
        while(*sharedCount == 0) {

        } //Se nao tem nada para ler, espera que o interpretador coloque algum processo.
        if (*sharedCount < 0) {
            break;
        }
        //fprintf(stderr, "\t->Tem arquivo pra ler na mem.\n");

        procs[numProc].procName = calloc(strlen(procName) + 1, sizeof(char));
        strcpy(procs[numProc].procName, procName);
        procs[numProc].priority = *priority;


        fprintf(stderr, "\t->procs[%d]: nome = %s, prioridade = %d\n", numProc, procs[numProc].procName, procs[numProc].priority);

        (*sharedCount)--;

        new_pid = fork();

        if(new_pid == 0) { //Processo filho.

            fprintf(stderr, "\t->Fork do filho %d\n", numProc);

            char* args[] = {procName, NULL};

            if ( execve(procName, args , NULL) == -1 ) {
                fprintf(stderr, "\t->Execve falhou!");
                perror("Nao executou :( \n");
                shmctl(mem1, IPC_RMID, 0);
                shmctl(mem2, IPC_RMID, 0);
                shmctl(mem3, IPC_RMID, 0);
            }
        }
        else { //Escalonador.

            procs[numProc].pid = new_pid;

            if (numProc == 0) { //Primeiro processo sempre entra direto, sem comparar.
                procs[numProc].ready = false;
                current = &procs[numProc];
                fprintf(stderr, "\t Corrente: %d\n", numProc);
            }
            else { //Nao sendo o primeiro, compara com o que esta rodando.

                fprintf(stderr, "\t -> Testando se proc %d tem mais prioridade que o corrente\n", numProc);

                if(current->priority > procs[numProc].priority) { //**********Confirmar se 1 eh a maior prioridade.
                    fprintf(stderr, "'E maior -> troca\n");
                    kill(current->pid, SIGSTOP);
                    current->ready = true;
                    current = &procs[numProc];
                    current->ready = false;
                    fprintf(stderr, "Corrente: %d\n", numProc);
                }
                else {
                    fprintf(stderr, "\t -> Nao 'e maior\n");

                    kill(procs[numProc].pid, SIGSTOP);
                    procs[numProc].ready = true;
                }
            }
        }

        numProc++;
        //fprintf(stderr, "\t->numProc: %d\n", numProc);
    }


//    fprintf(stderr, "Acabou de receber os processos\n");
    //Acabou de receber os processos, pode liberar a memoria.
    shmdt(procName);
    shmdt(priority);
    shmdt(sharedCount);

    shmctl(mem1, IPC_RMID, 0);
    shmctl(mem2, IPC_RMID, 0);
    shmctl(mem3, IPC_RMID, 0);



    fprintf(stderr, "\n\n   ========  \n->Escalonando já iniciados:\n");
    int totProc = numProc;
    //A partir de agora, deve lidar com os processos que ja estao executando. Se um terminar, chama o proximo por ordem de prioridade.
    //Achar uma forma de guardar os processo ja por ordem de prioridade?
    while(numProc > 0) {
        int status;
        fprintf(stderr, "\t->Comecei a esperar filho %s de PID: %d!\n", current->procName, current->pid);
        waitpid(current->pid, &status, 0); //Verificar chamada dessa funcao

        fprintf(stderr, "\t->Processo %s terminou!\n", current->procName);

        fprintf(stderr, "\t->Número de processos: %d!\n", numProc);

        current->ready = false;
        numProc--;

        int higherPriority = 8;
        Process* next = NULL;
        //CHAMA O PROXIMO COM MAIOR PRIORIDADE COM SIGCONT, DEFINE ELE COMO CURRENT
        for(int i = 0; i < totProc; i++) {
                if (procs[i].ready == false) {
                    continue;
                }
                if (procs[i].priority < higherPriority) {
                    next = &procs[i];
                    higherPriority = next->priority;
                }
        }



        if(next != NULL) {
            fprintf(stderr, "\t->Processo %s e o próximo de maior prioridade!\n", next->procName);
            current = next;
            kill(current->pid, SIGCONT);
            sleep(1);
            fprintf(stderr, "\t->Processo %s (re)iniciado!\n", current->procName);
        }

    }


    printf("Sou o escalonador e cheguei no final.\n");

    return 0;

}
//
// Created by danilo on 9/29/15.
//

