//
// Created by danilo on 9/29/15.
//
// Teste desse commit loko



#define _POSIX_SOURCE // apenas para código Linux não dar warning

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "auxiliary.h"
#include "command_list.h"
#include <sys/shm.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/types.h>


#include "process_list.h"

#define MAX_PROCNAME 10
#define TIME_SLICE 2


char* procName = NULL;
int mem1 = -1;
int parent  = 1;
Process* procs = NULL;
Process* current = NULL;
Process* processInIO  = NULL;

void callbackForInterrupt(int signal) {
    if (signal == SIGINT) {

        if (procName != NULL) {
            shmdt(procName);
        }

        if (mem1 > -1) {
            shmctl(mem1, IPC_RMID, 0);
        }

        if (procs != NULL) {
            free(procs);
        }

        exit(0);
    }
}

void callbackForIOStart(int s) {
    signal(SIGIO, callbackForIOStart);
    fprintf(stderr,"\n\nteste!\n\n");
    fprintf(stderr, "IO iniciaado por processo %s!!!%p\n", current->procName, current);
    assert(current != NULL);
    processInIO = current;
    current->ready = false;
    return;
}

void callbackForIOEnd(int s) {
    signal(SIGIOT, callbackForIOEnd);
    fprintf(stderr, "IO terminado por processo %s!!!%p\n", processInIO->procName, processInIO);
    processInIO->ready = true; // agora está pronto;
    processInIO = NULL;
    fprintf(stderr, "IO terminado com sucesso!");
}

int main(int argc, char* argv[])
{
    int numProc;
    int new_pid;
    int* priority;


    signal(SIGINT, callbackForInterrupt);
    //signal(SIG)

    signal(SIGIO, callbackForIOStart);
    signal(SIGIOT, callbackForIOEnd);


    //evita buffer de saída que pode ser copiado após fork.
    setvbuf(stdout, NULL, _IONBF, 0);

    //Mudando stdin/stdout para entrada.txt/saída.txt
    changeDefaultInputFile();
    changeDefaultOutputFile(1);

    printf("\nSou o escalonador! Meu PID é: %d\n", getpid());


    mem1    = atoi(argv[1]);
    numProc = atoi(argv[2]);

    printf("%d, %d\n", mem1, numProc);

    procName = (char*) shmat(mem1, 0, 0);

    procs = calloc((size_t) numProc, sizeof(Process));

    for (int i = 0; i < numProc; i++) {
        printf("Processo `%s`lido!\n", procName + i * MAX_PROCNAME);
        procs[i].procName = calloc(strlen(procName + i * MAX_PROCNAME) + 1, sizeof(char));

        strcpy(procs[i].procName, procName + i * MAX_PROCNAME);
        procs[i].ready = true;
        if (i >  0) { // linkando a lista (é um "vetor" que é uma lista também. Isso é bom pra facilitar deleção. Se necessário".)
            if (i + 1 != numProc) {
                procs[i - 1].next = procs + i;
            }
            procs[i].previous = procs + (i - 1);
        }

        procs[i].pid = -2;
    }

    printf("\n Fim da leitura dos processos, iniciando execução!!\n\n=================\n");
    int exec    = 0;

    int i       = 0;

    for (i = 0; i < numProc; i++) {
        if (!procs[i].ready) {
            continue;
        }

        if (current != NULL) {
            kill(current->pid, SIGSTOP);
            current->ready = true;
        }

        if (procs[i].pid == -2) { //primeira chamada de procs
            current = &procs[i]; //avoid NULL;
            new_pid = fork();
            if (new_pid == -1) { //erro
                perror("deu algum problema!!!");
                exit(0);
            }
            else if (new_pid > 0) { //pai
                printf("Escalonador diz: `%s` iniciado!\n", procs[i].procName);
                procs[i].pid = new_pid;
                procs[i].ready = false;

            }
            else if (new_pid == 0) { //filho
                printf("\texecve de %s posição %d \n\n", procs[i].procName, i);
                if (execve(procs[i].procName, NULL, NULL) == -1){
                    fprintf(stderr, "índice: %d\n", i);
                    perror("ops\n\n");
                    exit(1);
                };

            } // estou no filho, execucao termina.


        } else { //o processo ja havia sido iniciado antes, estava na fila de prontos ou terminado.
            printf("Filho `%s` já iniciado!\n", procs[i].procName);
            kill(procs[i].pid, SIGCONT);
            procs[i].ready = false;
            current = &procs[i];
        }


        if (i + 1 == numProc) { //voltar para o comeco1
            i = -1;
        }

       sleep(5);// time slice by now
    }

    free(procs);
    printf("\n -> Sou o escalonador e cheguei no final.\n");


    shmdt(procName);
    shmctl(mem1, IPC_RMID, 0);
    return 0;

}
