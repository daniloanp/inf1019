//
// Created by danilo on 9/29/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "auxiliary.h"
#include "command_list.h"
#include <sys/shm.h>
#include <fcntl.h>
#include <sys/wait.h>

#define MAX_PROCNAME 10

CommandList* parse(void)
{
    CommandList* l = CommandList_new();
    while (getc(stdin) != EOF) {
        fseek(stdin, -1, SEEK_CUR);
        Command c;
        char* err_char;
        char* priorityString;
        if (processToken("exec", " \n\t") == EOF){  //process token `exec`
            break; //acabou o arquivo;
        }
        c.proc_name = processString(" \n\t="); //
        if (strlen(c.proc_name) == 0) {
            printf("Nome do processo era esperado!");
            assert(0);
        }
        if (processToken("prioridade=", " \n\t") == EOF){ //process token `prioridade=`
            printf("Prioridade era esperada!");
            assert(0);
        }
        priorityString = processString(" \n\t");
        if (strlen(priorityString) == 0) {
            printf("Prioridade era esperada!");
            assert(0);
        }



        c.priority = (int)strtol(priorityString, &err_char, 10);

        if (err_char != NULL && *err_char != '\0') {
            fprintf(stderr, "Invalid priority given!");
            assert(0);
        }
        CommandList_insert(l, c);
    }
    return l;
}

void checkCommands(CommandList* list) {
    CommandListNode* node;
    Command cmd;
    struct stat sb;
    int ok = 1;
    for (node = list->first; node; node = node->next) {
        cmd = node->command;
        if (!(stat(cmd.proc_name, &sb) == 0 && sb.st_mode & S_IXUSR)) {
            fprintf(stderr, "Arquivo `%s` não existe ou não é executável!\n", cmd.proc_name);
            ok = 0;
        }
    }
    if (!ok) {
        fprintf(stderr, "Alguns Arquivos não são válidos!\n\n");
        assert(ok);
    }


    /* non-executable */
}

int main(void)
{

    printf("oi\n");
    int *priority, mem1, mem2, mem3, *sharedCount = 0;
    char* procName;
    Command cmd;
    cmd.proc_name = malloc(sizeof(char)*MAX_PROCNAME+1);

    //evita buffer de saída que pode ser copiado após fork.
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    pid_t pid;

    changeDefaultInputFile();
    changeDefaultOutputFile(0);

//    CommandList* commands = parse();
//    checkCommands(commands);


    key_t memKey1 = 8760;
    key_t memKey2 = 8752;
    key_t memKey3 = 8768;

    mem1 = shmget(memKey1, MAX_PROCNAME*sizeof(char), IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    if (mem1 == -1 ) {
        perror("Erro get memoria1 interpretador");
        exit(1);
    }

    mem2 = shmget(memKey2, sizeof(int), IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    if (mem2 == -1) {
        shmctl(mem1, IPC_RMID, 0);
        perror("Erro get memoria2 interpretador");
        exit(1);
    }

    mem3 = shmget(memKey3, sizeof(int),  IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    if (mem3 == -1) {
        shmctl(mem1, IPC_RMID, 0);
        shmctl(mem2, IPC_RMID, 0);
        perror("Erro get memoria3 interpretador");
        exit(1);
    }

    procName = (char*) shmat(mem1, 0, 0);
    priority = (int*) shmat(mem2, 0, 0);
    sharedCount = (int*) shmat(mem3, 0, 0);


    pid = fork();

    if(pid == 0) { //filho -> escalonador

        printf("Interpretador: inicializacao de argumentos para escalonador.\n");

        char arg1[31], arg2[31], arg3[31];
        sprintf(arg1,"%d", mem1);
        sprintf(arg2,"%d", mem2);
        sprintf(arg3,"%d", mem3);

        printf("%s\n", arg1);
        printf("%s\n", arg2);
        printf("%s\n", arg3);



        char* const args[] = {"escalonador_prioridade", arg1,arg2,arg3,NULL};

        printf("Interpretador: chamada de escalonador.\n");
        if ( execve("./escalonador_prioridade", args ,NULL) == -1 ) {
            perror("Nao executou :( \n");
            shmctl(mem1, IPC_RMID, 0);
            shmctl(mem2, IPC_RMID, 0);
            shmctl(mem3, IPC_RMID, 0);
        }

    }
    else { //pai -> interpretador
        char line[255]; //tamanho maximo de linha;
        while (gets(line)!= NULL) { //fazer o parse linha a linha (lê uma linha, espera 't' tempo). (while nao for EOF)

            fprintf(stderr,"Interpretador começou a esperar! (%s) :sc = %d \n", line, *sharedCount);
            char *s = line;
            while(*sharedCount == 1) { } //espera se a memoria compartilhada estiver cheia
            fprintf(stderr,"Interpretador terminou de esperar! (%s) :sc = %d \n", line,*sharedCount);

            char * exec = "exec ";
            char * prioridade = "prioridade=";
            while(*exec != '\0') {
                if (*exec != *s) {
                    perror("Arquivo invalido!\n");
                    assert(0);
                }
                s++;
                exec++;
            }

            sscanf(s, "%s", cmd.proc_name);
            s += strlen(cmd.proc_name) +1;

            while(*prioridade != '\0') {
                if (*prioridade != *s) {
                    perror("Arquivo invalido!\n");
                    assert(0);
                }
                s++;
                prioridade++;
            }

            sscanf(s, "%d", &cmd.priority );


            fprintf(stderr, "%s %d \n", cmd.proc_name, cmd.priority);

            strcpy(procName, cmd.proc_name); //mem compartilhada esvaziou, posso colocar o que li la.
            *priority = cmd.priority;

            (*sharedCount)++;

            fprintf(stderr, "cheguei aqui!!!\n");
            sleep(3);
        }

        fprintf(stderr, "Interpretador terminou de ler! Vai esperar escalonador ler o ultimo processo.\n");

        while (*sharedCount == 1) { }

        fprintf(stderr, "Escalonador leu o ultimo processo, interpretador esta finalizando.\n");

        *sharedCount = -1;
        // Posso acabar o processo pai antes de acabar o filho? Se sim, acabou o interpretador aqui.
        int status;
        waitpid(pid, &status, 0);
    }

    return 0;
}
