#define _POSIX_SOURCE // apenas para código Linux não dar warning

#include <unistd.h>
#include <stdio.h>

#define EVER (;;)

int main(void) {
    for (int i = 0; i < 9; i++) {
        printf("\t Sétimo! %d! %d\n", i, getpid());
        sleep(2);
    };
    return 0;
}
